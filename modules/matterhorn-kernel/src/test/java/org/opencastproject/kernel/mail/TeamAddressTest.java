/**
 *  Copyright 2009, 2010 The Regents of the University of California
 *  Licensed under the Educational Community License, Version 2.0
 *  (the "License"); you may not use this file except in compliance
 *  with the License. You may obtain a copy of the License at
 *
 *  http://www.osedu.org/licenses/ECL-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an "AS IS"
 *  BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 *  or implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 */
package org.opencastproject.kernel.mail;

import static junit.framework.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.apache.commons.validator.routines.EmailValidator;
import org.junit.Test;

/**
 * @author John Crossman
 */
public class TeamAddressTest {

  @Test
  public void testValidAddresses() {
    final EmailValidator validator = EmailValidator.getInstance(true);
    for (final TeamAddress teamAddress : TeamAddress.values()) {
      assertTrue(validator.isValid(teamAddress.getAddress()));
      final String recipientName = teamAddress.getRecipientName();
      assertNotNull(recipientName);
      assertTrue(recipientName.length() < 50);
      assertTrue(Character.isUpperCase(recipientName.charAt(0)));
    }
  }

}
