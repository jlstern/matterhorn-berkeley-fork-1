/**
 *  Copyright 2009, 2010 The Regents of the University of California
 *  Licensed under the Educational Community License, Version 2.0
 *  (the "License"); you may not use this file except in compliance
 *  with the License. You may obtain a copy of the License at
 *
 *  http://www.osedu.org/licenses/ECL-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an "AS IS"
 *  BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 *  or implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 */
package org.opencastproject.job.api;

import org.opencastproject.util.env.EnvironmentUtil;

import org.osgi.service.cm.ConfigurationException;
import org.osgi.service.cm.ManagedService;

import java.util.Dictionary;

/**
 * @author John Crossman
 */
public abstract class AbstractManagedServiceJobProducer extends AbstractJobProducer implements ManagedService {

  /**
   * @see AbstractJobProducer#AbstractJobProducer(String)
   * @param jobType Null not allowed.
   */
  protected AbstractManagedServiceJobProducer(final String jobType) {
    super(jobType);
  }

  /**
   * Sub-classes will inherit the feature provided by {@link org.opencastproject.util.env.EProperties}
   * This feature is backwards-compatible with the way standard properties are loaded.
   * @see ManagedService#updated(java.util.Dictionary)
   */
  public final synchronized void updated(final Dictionary properties) throws ConfigurationException {
    updatedConfiguration(EnvironmentUtil.createEProperties(properties, true));
  }

  /**
   * If sub-class does not implement {@link org.osgi.service.cm.ManagedService} then implement this method as an
   * empty method.
   * @see ManagedService#updated(java.util.Dictionary)
   * @param properties Null not allowed.
   * @throws ConfigurationException per method referenced above.
   */
  protected abstract void updatedConfiguration(Dictionary properties) throws ConfigurationException;

}
