/**
 *  Copyright 2009, 2010 The Regents of the University of California
 *  Licensed under the Educational Community License, Version 2.0
 *  (the "License"); you may not use this file except in compliance
 *  with the License. You may obtain a copy of the License at
 *
 *  http://www.osedu.org/licenses/ECL-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an "AS IS"
 *  BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 *  or implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 */
package org.opencastproject.util.env;

import org.opencastproject.util.XProperties;

import org.apache.commons.lang.StringUtils;

import java.util.Dictionary;
import java.util.Enumeration;

/**
 * Properties filtered based on value of {@link EnvironmentUtil#getEnvironment()}. We look for keys with format of:
 * <ol>
 *   <li>intendedKeyName + "_" + {@link EnvironmentUtil#getEnvironment()}</li>
 *   <li>intendedKeyName</li>
 * </ol>
 * <b>Note:</b> The list above is ordered by precedence. If first key returns a value then entries with second key are
 * ignored.
 * @author John Crossman
 */
public class EProperties extends XProperties {

  /**
   * Package-local constructor. Use {@link EnvironmentUtil#createEProperties(java.util.Dictionary, boolean)}.
   * @param d will be wrapped.
   */
  EProperties(final Dictionary d) {
    final Environment environment = EnvironmentUtil.getEnvironment();
    final Enumeration keys = d.keys();
    while (keys.hasMoreElements()) {
      final Object object = keys.nextElement();
      // Next element should always be a String but better safe than sorry.
      if (object instanceof String) {
        final String key = (String) object;
        final String suffix = StringUtils.substringAfter(key, "_");
        final Environment match = (suffix == null) ? null : EnvironmentUtil.findMatch(suffix);
        if (match == null) {
          final boolean putThisProperty = null == d.get(key + '_' + environment.name());
          // Only put this default property (i.e., no underscore plus custom suffix) if a we DO NOT FIND a property
          // value specific to KEY_MATTERHORN_ENV setting.  For example, if KEY_MATTERHORN_ENV=qa and the property file
          // does NOT contain the key "foo_qa" then we will go ahead and use property of "foo".
          if (putThisProperty) {
            setProperty(key, (String) d.get(key));
          }
        } else if (match == environment) {
          // Property targeted for this runtime environment.
          final String keyBeforeUnderscore = StringUtils.substringBeforeLast(key, "_");
          remove(keyBeforeUnderscore);
          setProperty(keyBeforeUnderscore, (String) d.get(key));
        }
      }
    }
    merge(EnvironmentUtil.getRuntimePropertyOverrides());
  }

}
