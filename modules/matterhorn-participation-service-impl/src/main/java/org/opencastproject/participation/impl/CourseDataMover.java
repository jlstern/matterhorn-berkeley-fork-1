/**
 *  Copyright 2009, 2010 The Regents of the University of California
 *  Licensed under the Educational Community License, Version 2.0
 *  (the "License"); you may not use this file except in compliance
 *  with the License. You may obtain a copy of the License at
 *
 *  http://www.osedu.org/licenses/ECL-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an "AS IS"
 *  BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 *  or implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 */
package org.opencastproject.participation.impl;

import org.opencastproject.notify.Notifier;
import org.opencastproject.participation.api.CourseManagementService;
import org.opencastproject.participation.impl.persistence.CourseDatabaseException;
import org.opencastproject.participation.impl.persistence.ProvidesCourseCatalogData;
import org.opencastproject.participation.impl.persistence.UCBerkeleyCourseDatabaseImpl;
import org.opencastproject.participation.model.CourseData;
import org.opencastproject.participation.model.Instructor;
import org.opencastproject.participation.model.Room;
import org.opencastproject.participation.model.Term;
import org.opencastproject.util.NotFoundException;

import org.apache.commons.lang.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.TimerTask;

/**
 * @author John Crossman
 */
public class CourseDataMover extends TimerTask {

  private final char br = '\n';
  private Logger logger = LoggerFactory.getLogger(this.getClass());

  private Notifier notifier;
  private ProvidesCourseCatalogData courseDatabase;
  private CourseManagementService courseManagementService;
  private boolean throwExceptionInRunMethod = false;

  public CourseDataMover() {
  }

  public CourseDataMover(final Logger logger) {
    this.logger = logger;
  }

  @Override
  public void run() {
    final Set<Term> termSet = courseManagementService.getAllTerms();
    final List<CourseData> coursesUpdated = new LinkedList<CourseData>();
    final StringBuilder message = new StringBuilder();
    boolean sendEmail = false;
    try {
      if (termSet.size() == 0) {
        message.append(br).append("[WARN] Zero terms in 'not-closed' stage returned from Salesforce.").append(br).append(br);
      } else {
        final List<Room> allRooms = new LinkedList<Room>(courseManagementService.getAllRooms());
        for (final Term term : termSet) {
          final Set<CourseData> courseData = courseDatabase.getCourseData(term.getSemester(), term.getTermYear());
          final Set<CourseData> validCourseData = CourseUtils.removeCoursesWithUnrecognizedRooms(courseData, allRooms);
          coursesUpdated.addAll(createOrUpdateCourses(validCourseData, term, allRooms, message));
        }
      }
    } catch (final Exception e) {
      appendException(message, e);
      sendEmail = true;
      if (throwExceptionInRunMethod) {
        // We do not rethrow the exception because that will invoke cancel() on the Timer instance.
        throw new CourseDatabaseException(message.toString(), e);
      }
    } finally {
      logger.info(message.toString());
      // We do NOT send email if FileBasedCourseCatalogData returns zero results because that job runs once per minute.
      sendEmail = sendEmail || coursesUpdated.size() > 0 || courseDatabase instanceof UCBerkeleyCourseDatabaseImpl;
      if (sendEmail) {
        final String whenRun = new SimpleDateFormat("yyyy-MM-dd HH:mm").format(new Date());
        final String subject = "CourseDataMover (" + courseDatabase.getClass().getSimpleName() + "), " + whenRun;
        notifier.notifyEngineeringTeam(subject, message.toString());
      }
    }
  }

  private Collection<CourseData> createOrUpdateCourses(final Set<CourseData> incomingCourseData, final Term term, final List<Room> allRooms,
                                                       final StringBuilder message) throws NotFoundException {
    final List<CourseData> coursesUpdated = new LinkedList<CourseData>();
    // Create or update instructors attached to courses. Plus, put salesforce IDs back to courses.
    final Set<Instructor> instructorSet = CourseUtils.extractInstructorSet(incomingCourseData);
    final List<Instructor> instructorListFromSalesforce = courseManagementService.createOrUpdateInstructors(instructorSet);
    CourseUtils.setSalesforceIDs(incomingCourseData, allRooms, instructorListFromSalesforce, term);
    courseManagementService.createOrUpdateCourses(incomingCourseData);
    message.append("[INFO] ").append(incomingCourseData.size()).append(" courses in capture-enabled.").append(br).append(br);
    int courseIndex = 1;
    for (final CourseData courseData : incomingCourseData) {
      message.append(courseIndex++).append(". ").append(courseData).append(br).append(br);
    }
    coursesUpdated.addAll(incomingCourseData);
    return coursesUpdated;
  }

  public void setCourseDatabase(final ProvidesCourseCatalogData courseDatabase) {
    this.courseDatabase = courseDatabase;
  }

  public void setCourseManagementService(final CourseManagementService courseManagementService) {
    this.courseManagementService = courseManagementService;
  }

  public void setNotifier(final Notifier notifier) {
    this.notifier = notifier;
  }

  public void setThrowExceptionInRunMethod(final boolean throwExceptionInRunMethod) {
    this.throwExceptionInRunMethod = throwExceptionInRunMethod;
  }

  private void appendException(final StringBuilder b, final Exception e) {
    b.append("Exception class: ").append(e.getClass().getSimpleName()).append('\n');
    b.append("Exception message: ").append(e.getMessage()).append('\n');
    b.append(ExceptionUtils.getFullStackTrace(e)).append('\n');
  }

}
