/**
 *  Copyright 2009, 2010 The Regents of the University of California
 *  Licensed under the Educational Community License, Version 2.0
 *  (the "License"); you may not use this file except in compliance
 *  with the License. You may obtain a copy of the License at
 *
 *  http://www.osedu.org/licenses/ECL-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an "AS IS"
 *  BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 *  or implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 */
package org.opencastproject.participation.impl;

import org.apache.commons.lang.StringUtils;
import org.opencastproject.participation.model.CourseData;
import org.opencastproject.participation.model.Instructor;
import org.opencastproject.participation.model.Participation;
import org.opencastproject.participation.model.Room;
import org.opencastproject.participation.model.Term;
import org.opencastproject.participation.mvc.Message;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * @author John King
 */
public final class CourseUtils {

  /**
   * Private.
   */
  private CourseUtils() {
  }

  /**
   * @see org.opencastproject.participation.model.CourseData#getParticipationSet()
   * @param courseDataSet never null
   * @return zero or more instructors extracted from list of courses
   */
  public static Set<Instructor> extractInstructorSet(final Set<CourseData> courseDataSet) {
    final Set<Instructor> instructorSet = new HashSet<Instructor>();
    for (final CourseData courseData : courseDataSet) {
      final Set<Participation> participationList = courseData.getParticipationSet();
      for (final Participation participation : participationList) {
        final Instructor instructor = participation.getInstructor();
        if (instructor == null) {
          throw new IllegalStateException("Null instructor on participation of courseData: " + courseData);
        }
        instructorSet.add(instructor);
      }
    }
    return instructorSet;
  }

  /**
   * @param room Null allowed
   * @return true if room and {@link org.opencastproject.participation.model.Room#getCapability()} are not null
   */
  public static boolean isRoomCapable(final Room room) {
    return room != null && room.getCapability() != null;
  }

  /**
   * @see org.opencastproject.salesforce.HasSalesforceId#getSalesforceID()
   * @param courseDataSet Courses with presumably null salesforceID
   * @param allRooms list pulled from Salesforce. All entries must have non-null salesforceID
   * @param instructorList list pulled from Salesforce. All entries must have non-null salesforceID
   * @param term must have non-null salesforceID
   */
  public static void setSalesforceIDs(final Set<CourseData> courseDataSet, final List<Room> allRooms,
                                      final List<Instructor> instructorList, final Term term) {
    final StringBuilder b = new StringBuilder();
    for (final CourseData course : courseDataSet) {
      course.getTerm().setSalesforceID(term.getSalesforceID());
      final int indexOfMatch = allRooms.indexOf(course.getRoom());
      if (indexOfMatch > -1) {
        final Room roomFromSalesforce = allRooms.get(indexOfMatch);
        course.getRoom().setSalesforceID(roomFromSalesforce.getSalesforceID());
      }
      final Set<Participation> participationSet = course.getParticipationSet();
      for (final Participation p : participationSet) {
        final Instructor incoming = p.getInstructor();
        final int indexOf = instructorList.indexOf(incoming);
        final Instructor fromSalesforce = instructorList.get(indexOf);
        incoming.setSalesforceID(fromSalesforce.getSalesforceID());
      }
      if (course.getTerm().getSalesforceID() == null) {
        appendCourseRelatedErrorMessage(b, course, " term");
      }
      if (course.getRoom().getSalesforceID() == null) {
        appendCourseRelatedErrorMessage(b, course, " room " + course.getRoom());
      }
      for (final Participation p : course.getParticipationSet()) {
        if (p.getInstructor().getSalesforceID() == null) {
          appendCourseRelatedErrorMessage(b, course, " instructor " + p.getInstructor());
        }
      }
    }
    if (b.length() > 0) {
      throw new IllegalStateException(StringUtils.abbreviate(b.toString(), 200));
    }
  }

  /**
   * @param courseDataSet Null not allowed
   * @param knownRooms Null not allowed
   * @return courses with room objects that have a match within list of known rooms
   */
  public static Set<CourseData> removeCoursesWithUnrecognizedRooms(final Set<CourseData> courseDataSet, final List<Room> knownRooms) {
    final Set<CourseData> validCourseDataSet = new HashSet<CourseData>();
    for (final CourseData courseData : courseDataSet) {
      if (knownRooms.contains(courseData.getRoom())) {
        validCourseDataSet.add(courseData);
      }
    }
    return validCourseDataSet;
  }

  private static void appendCourseRelatedErrorMessage(final StringBuilder b, final CourseData course, final String messageSuffix) {
    b.append(course.getCanonicalCourse().getCcn()).append(" missing salesforceID on ").append(messageSuffix);
  }

  public static String getTimeForDisplay(final String militaryTime) {

    return null;
  }
}
