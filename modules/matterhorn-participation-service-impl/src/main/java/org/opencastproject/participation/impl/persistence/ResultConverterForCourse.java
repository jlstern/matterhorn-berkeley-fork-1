/**
 *  Copyright 2009, 2010 The Regents of the University of California
 *  Licensed under the Educational Community License, Version 2.0
 *  (the "License"); you may not use this file except in compliance
 *  with the License. You may obtain a copy of the License at
 *
 *  http://www.osedu.org/licenses/ECL-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an "AS IS"
 *  BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 *  or implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 */
package org.opencastproject.participation.impl.persistence;

import org.apache.commons.lang.StringUtils;

import org.opencastproject.participation.impl.MVUtils;
import org.opencastproject.participation.impl.MVUtils.DateCol;
import org.opencastproject.participation.impl.MVUtils.IntCol;
import org.opencastproject.participation.impl.MVUtils.StrCol;
import org.opencastproject.participation.model.CanonicalCourse;
import org.opencastproject.participation.model.CourseData;
import org.opencastproject.participation.model.Room;
import org.opencastproject.participation.model.Semester;
import org.opencastproject.participation.model.Term;
import org.opencastproject.util.date.DateUtil;
import org.opencastproject.util.date.TimeOfDay;
import org.opencastproject.salesforce.SalesforceUtils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.MessageFormat;
import java.text.SimpleDateFormat;

/**
 * @author John Crossman
 */
public class ResultConverterForCourse implements ResultSetRowConverter<CourseData> {

  private final Logger logger = LoggerFactory.getLogger(this.getClass());

  @Override
  public CourseData convert(final ResultSet r) throws SQLException {
    final String catalogId = MVUtils.get(r, StrCol.catalog_id);
    final String sectionNum = MVUtils.get(r, StrCol.section_num);
    final CourseData c = new CourseData();
    c.setCanonicalCourse(new CanonicalCourse());
    c.getCanonicalCourse().setCcn(MVUtils.get(r, IntCol.course_cntl_num));
    final String deptDescription = MVUtils.get(r, StrCol.dept_description);
    c.getCanonicalCourse().setDepartment(deptDescription);
    final String formattdCatalogId = StringUtils.isBlank(catalogId) ? "," : " " + catalogId + ",";
    final String title = MessageFormat.format("{0}{1} {2}", deptDescription, formattdCatalogId, sectionNum);
    c.getCanonicalCourse().setTitle(title);
    final Term term = new Term();
    final SimpleDateFormat expectedFormat = SalesforceUtils.getDateFormatConvention();
    term.setSemesterStartDate(expectedFormat.format(MVUtils.get(r, DateCol.term_start_date)));
    term.setSemesterEndDate(expectedFormat.format(MVUtils.get(r, DateCol.term_end_date)));
    final String semesterName = MVUtils.get(r, StrCol.term_name);
    term.setSemester(SalesforceUtils.findSalesforceMatch(semesterName, Semester.values()));
    term.setTermYear(Integer.parseInt(MVUtils.get(r, StrCol.term_yr)));
    c.setTerm(term);
    //
    final TimeOfDay startTime = getTimeOfDay(MVUtils.get(r, StrCol.meeting_start_time), MVUtils.get(r, StrCol.meeting_start_time_ampm_flag));
    c.setStartTime(DateUtil.getMilitaryTime(startTime));
    final TimeOfDay endTime = getTimeOfDay(MVUtils.get(r, StrCol.meeting_end_time), MVUtils.get(r, StrCol.meeting_end_time_ampm_flag));
    c.setEndTime(DateUtil.getMilitaryTime(endTime));
    //
    final Room room = new Room();
    final String building = MVUtils.get(r, StrCol.building_name);
    if (StringUtils.containsIgnoreCase(building, "wheeler") && StringUtils.containsIgnoreCase(building, "aud")) {
      room.setBuilding("Wheeler");
      room.setRoomNumber("150");
    } else {
      room.setBuilding(StringUtils.trimToNull(building));
      final String roomNumber = parseRoomNumber(MVUtils.get(r, StrCol.room_number));
      room.setRoomNumber(roomNumber);
    }
    c.setRoom(room);
    if (room.getRoomNumber() == null) {
      logger.warn("Null room number for CCN #" + c.getCanonicalCourse().getCcn() + " during " + term);
    }
    //
    c.setMeetingDays(MVUtils.getMeetingDays(MVUtils.get(r, StrCol.meeting_days)));
    c.setSection(sectionNum);
    c.setStudentCount(MVUtils.get(r, IntCol.student_count));
    return c;
  }

  /**
   * Package-local for sake of unit test.
   * @param hoursMinutes for example, "0830", "1100", "0500", etc.
   * @param amPmFlag "A" for AM and "P" for PM
   * @return appropriate according to rules above
   */
  static TimeOfDay getTimeOfDay(final String hoursMinutes, final String amPmFlag) {
    final TimeOfDay timeOfDay;
    if (StringUtils.isBlank(hoursMinutes) || StringUtils.isBlank(amPmFlag)) {
      timeOfDay = null;
    } else {
      final Integer hoursMinutesNumber = Integer.parseInt(hoursMinutes);
      final int hour = hoursMinutesNumber / 100;
      final int minutes = (hoursMinutesNumber % 100);
      final TimeOfDay.DayPeriod dayPeriod = ("A".equalsIgnoreCase(amPmFlag)) ? TimeOfDay.DayPeriod.AM : TimeOfDay.DayPeriod.PM;
      timeOfDay = new TimeOfDay(hour, minutes, dayPeriod);
    }
    return timeOfDay;
  }

  /**
   * Package-local for sake of unit test.
   * @param roomNumberString for example: '0358A', ' D0025', '0009', etc.
   * @return extracted number
   */
  private String parseRoomNumber(final String roomNumberString) {
    final String parsed;
    if (StringUtils.isBlank(roomNumberString)) {
      parsed = null;
    } else if (StringUtils.isNumeric(roomNumberString)) {
      parsed = Integer.valueOf(roomNumberString).toString();
    } else {
      parsed = StringUtils.trimToNull(roomNumberString);
    }
    return parsed;
  }
}
