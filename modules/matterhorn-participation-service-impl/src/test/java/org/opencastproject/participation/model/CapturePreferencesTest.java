/**
 *  Copyright 2009, 2010 The Regents of the University of California
 *  Licensed under the Educational Community License, Version 2.0
 *  (the "License"); you may not use this file except in compliance
 *  with the License. You may obtain a copy of the License at
 *
 *  http://www.osedu.org/licenses/ECL-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an "AS IS"
 *  BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 *  or implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 */

package org.opencastproject.participation.model;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class CapturePreferencesTest {

  @Before
  public void setUp() throws Exception {
    aCapturePreferences.setRecordingType(RecordingType.valueOf(RECORDING_TYPE));
    aCapturePreferences.setRecordingAvailability(RecordingAvailability.valueOf(RECORDING_AVAILABILITY));
    aCapturePreferences.setDelayPublishByDays(Integer.parseInt(DELAY_PUBLISH_BY_DAYS));
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void testCapturePreferencesStringStringString() {
    CapturePreferences capturePreferences = new CapturePreferences(RECORDING_TYPE, RECORDING_AVAILABILITY, DELAY_PUBLISH_BY_DAYS);
      assertThat(aCapturePreferences.getRecordingType(), equalTo(capturePreferences.getRecordingType()));
      assertThat(aCapturePreferences.getRecordingAvailability(), equalTo(capturePreferences.getRecordingAvailability()));
      assertThat(aCapturePreferences.getDelayPublishByDays(), equalTo(capturePreferences.getDelayPublishByDays()));
    }
  
  private static final String RECORDING_TYPE = "videoAndScreencast";
  private static final String RECORDING_AVAILABILITY = "publicCreativeCommons";
  private static final String DELAY_PUBLISH_BY_DAYS = "1";
  private CapturePreferences aCapturePreferences = new CapturePreferences();
  
  }