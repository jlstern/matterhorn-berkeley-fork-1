/**
 *  Copyright 2009, 2010 The Regents of the University of California
 *  Licensed under the Educational Community License, Version 2.0
 *  (the "License"); you may not use this file except in compliance
 *  with the License. You may obtain a copy of the License at
 *
 *  http://www.osedu.org/licenses/ECL-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an "AS IS"
 *  BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 *  or implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 */
package org.opencastproject.participation.impl;

import org.opencastproject.participation.ConsoleLogger;
import org.opencastproject.participation.UnitTestUtils;
import org.opencastproject.participation.impl.persistence.UCBerkeleyCourseDatabaseImpl;
import org.opencastproject.participation.model.CourseData;
import org.opencastproject.participation.model.Room;
import org.opencastproject.participation.model.Semester;
import org.opencastproject.salesforce.SalesforceConnectorServiceImpl;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.osgi.service.cm.ConfigurationException;
import org.slf4j.Logger;

import java.io.IOException;
import java.util.Set;

/**
 * @author John Crossman
 */
@Ignore
public class CourseDataMoverMaterialViewsTest {

  private static final Logger logger = new ConsoleLogger(CourseDataMoverMaterialViewsTest.class);

  private CourseDataMover dataMover = new CourseDataMover(logger);
  private UCBerkeleyCourseDatabaseImpl courseDatabase;
  private CourseManagementServiceImpl courseManagementService;

  @Before
  public void before() throws IOException, ConfigurationException {
    final VoidNotifier notifier = new VoidNotifier(logger);
    courseManagementService = new CourseManagementServiceImpl(logger);
    final SalesforceConnectorServiceImpl connectorService = new SalesforceConnectorServiceImpl(logger);
    connectorService.setNotifier(notifier);
    connectorService.updated(UnitTestUtils.getSalesforceConnectionProperties());
    courseManagementService.setSalesforceConnectorService(connectorService);
    //
    dataMover.setCourseManagementService(courseManagementService);
    courseDatabase = new UCBerkeleyCourseDatabaseImpl(UnitTestUtils.getCourseDatabaseCredentials());
    dataMover.setCourseDatabase(courseDatabase);
    dataMover.setNotifier(notifier);
    dataMover.setThrowExceptionInRunMethod(true);
  }

  @Test
  public void testRun() {
    dataMover.run();
  }

  @Ignore
  @Test
  public void testAllRooms() {
    final Set<Room> allRooms = courseManagementService.getAllRooms();
    for (final Room room : allRooms) {
      logger.info(room.toString());
    }
    System.out.println("=============================");
    final Set<CourseData> courseDataSet = courseDatabase.getCourseData(Semester.Fall, 2013);
    for (final CourseData courseData : courseDataSet) {
      logger.info(courseData.getRoom().toString());
      logger.info("      allRooms.contains? " + allRooms.contains(courseData.getRoom()));
    }
  }
}
