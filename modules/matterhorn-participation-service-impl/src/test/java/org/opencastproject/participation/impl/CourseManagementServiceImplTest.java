/**
 *  Copyright 2009, 2010 The Regents of the University of California
 *  Licensed under the Educational Community License, Version 2.0
 *  (the "License"); you may not use this file except in compliance
 *  with the License. You may obtain a copy of the License at
 *
 *  http://www.osedu.org/licenses/ECL-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an "AS IS"
 *  BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 *  or implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 */
package org.opencastproject.participation.impl;

import static junit.framework.Assert.assertNull;
import static org.easymock.EasyMock.anyObject;
import static org.easymock.EasyMock.createMock;
import static org.easymock.EasyMock.expect;
import static org.easymock.EasyMock.expectLastCall;
import static org.easymock.EasyMock.replay;

import org.opencastproject.participation.model.CanonicalCourse;
import org.opencastproject.participation.model.CapturePreferences;
import org.opencastproject.participation.model.CourseData;
import org.opencastproject.participation.model.CourseOffering;
import org.opencastproject.participation.model.RecordingAvailability;
import org.opencastproject.participation.model.Instructor;
import org.opencastproject.participation.model.License;
import org.opencastproject.participation.model.Participation;
import org.opencastproject.participation.model.RecordingType;
import org.opencastproject.participation.model.Room;
import org.opencastproject.participation.model.Semester;
import org.opencastproject.participation.model.Term;
import org.opencastproject.salesforce.SalesforceConnectorService;
import org.opencastproject.salesforce.SalesforceObjectTransformer;
import org.opencastproject.salesforce.SalesforceQueryType;
import org.opencastproject.util.NotFoundException;
import org.opencastproject.util.data.Collections;

import com.sforce.soap.partner.QueryResult;
import com.sforce.soap.partner.SaveResult;
import com.sforce.soap.partner.sobject.SObject;
import com.sforce.ws.ConnectionException;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.osgi.service.cm.ConfigurationException;

import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @author John Crossman
 */
public class CourseManagementServiceImplTest {

  private CourseManagementServiceImpl courseManagementService;
  private SalesforceConnectorService connectorService;
  private SalesforceObjectTransformer objectTransformer;

  @Before
  public void before() throws ConfigurationException, NotFoundException {
    courseManagementService = new CourseManagementServiceImpl();
    connectorService = createMock(SalesforceConnectorService.class);
    objectTransformer = createMock(SalesforceObjectTransformer.class);
    courseManagementService.setSalesforceConnectorService(connectorService);
    courseManagementService.setSalesforceObjectTransformer(objectTransformer);
  }

  @Test
  public void testCreateOrUpdateCoursesNoUpdates() {
    // Expect zero calls.
    replay(connectorService, objectTransformer);
    courseManagementService.createOrUpdateCourses(new HashSet<CourseData>());
  }

  @Test
  public void testGetCourseOfferingNull() {
    expect(connectorService.query(anyObject(SalesforceQueryType.class), anyObject(Map.class))).andReturn(new QueryResult()).once();
    replay(connectorService, objectTransformer);
    assertNull(courseManagementService.getCourseOffering("bad-input"));
  }

  @Ignore
  @Test
  public void testCreateTermThenCreateCourses() throws ConnectionException {
    // Semester
    final Term term = new Term();
    term.setTermYear(2014);
    term.setSemester(Semester.Fall);
    term.setSemesterStartDate("2014-08-20");
    term.setSemesterEndDate("2014-12-10");
    // Room
    final Room room = new Room();
    room.setBuilding("building");
    room.setRoomNumber("1");
    room.setSalesforceID(room.hashCode() + "");
    // Course #1
    final CourseOffering c1 = createCourseOffering();
    c1.setParticipationSet(new HashSet<Participation>());
    c1.setTerm(term);
    c1.setParticipationSet(new HashSet<Participation>());
    c1.setRoom(room);
    c1.setSalesforceID("salesforceID-" + c1.hashCode());
    // Course #2
    final CourseOffering c2 = createCourseOffering();
    c2.setTerm(term);
    c2.setParticipationSet(new HashSet<Participation>());
    c2.setRoom(room);
    c2.setSalesforceID("salesforceID-" + c2.hashCode());
    // Zero terms, zero instructors, one room
    final QueryResult termResult = new QueryResult();
    final QueryResult instructorResult = new QueryResult();
    final QueryResult roomResult = getSingleResult();
    expect(connectorService.query(SalesforceQueryType.selectAllTerms, null)).andReturn(termResult).once();
    expect(connectorService.query(SalesforceQueryType.selectAllInstructors, null)).andReturn(instructorResult).once();
    expect(connectorService.query(SalesforceQueryType.selectAllRooms, null)).andReturn(roomResult).once();
    //
    final QueryResult courseOfferingResult = new QueryResult();
    final SObject[] courseOfferingRecords = new SObject[0];
    courseOfferingResult.setRecords(courseOfferingRecords);
    //
    final Map<String, String> args = new HashMap<String, String>();
    args.put("semesterYear", term.getTermYear().toString());
    args.put("semesterTerm", Semester.Fall.name());
    expect(connectorService.query(SalesforceQueryType.selectCourseOfferingsBySemesterYear, args)).andReturn(courseOfferingResult).once();
    expect(connectorService.create(new LinkedList<SObject>())).andReturn(new SaveResult[1]).once();
    //
    final Map<CourseOffering, SObject> map = new HashMap<CourseOffering, SObject>();
    final SObject so1 = new SObject();
    final SObject so2 = new SObject();
    map.put(c1, so1);
    map.put(c2, so2);
    //
    expect(objectTransformer.extractAssociatedCourseOfferings(courseOfferingRecords, Semester.Fall, term.getTermYear())).andReturn(map).once();
    expect(objectTransformer.getSObjectForCourseCreation(c1)).andReturn(so1).once();
    expect(objectTransformer.getSObjectForCourseCreation(c2)).andReturn(so2).once();
//    expect(objectTransformer.getTerm(termResult.getRecords()[0])).andReturn(term).once();
    expect(objectTransformer.getRoom(roomResult.getRecords()[0])).andReturn(room).once();
//    expect(objectTransformer.getInstructor(instructorResult.getRecords()[0])).andReturn(instructor).once();
    //
    objectTransformer.setFieldsForCourseUpdate(so1, c1);
    expectLastCall().once();
    objectTransformer.setFieldsForCourseUpdate(so2, c2);
    expectLastCall().once();
    //
    expect(connectorService.update(so1)).andReturn(mockSaveResults(1)).once();
    expect(connectorService.update(so2)).andReturn(mockSaveResults(1)).once();
    //
    replay(connectorService, objectTransformer);
    //
    final Set<CourseData> set = new HashSet<CourseData>();
    set.add(c1);
    set.add(c2);
    courseManagementService.createOrUpdateCourses(set);
  }

  private QueryResult getSingleResult() {
    final QueryResult r = new QueryResult();
    final SObject s = new SObject();
    s.setId(s.hashCode() + "");
    r.setRecords(new SObject[] { s });
    return r;
  }

  private SaveResult[] mockSaveResults(final int count) {
    final SaveResult[] mock = new SaveResult[count];
    for (int index = 0; index < count; index++) {
      mock[index] = new SaveResult();
    }
    return mock;
  }

  @Test(expected = IllegalStateException.class)
  public void testUpdateParticipation() throws NotFoundException, ConnectionException {
    testUpdateParticipation(true);
  }

  @Test(expected = IllegalStateException.class)
  public void testUnauthorizedParticipationUpdate() throws NotFoundException, ConnectionException {
    testUpdateParticipation(false);
  }

  @Ignore
  @Test
  public void testSetRecordingsScheduledTrue() throws NotFoundException {
    final CourseOffering c = createCourseOffering();
    courseManagementService.setRecordingsScheduledTrue(c.getCourseOfferingId());
//    final CourseOffering offering = courseManagementService.selectCourseOffering(c);
//    assertTrue(offering.isScheduled());
  }

  private void testUpdateParticipation(final boolean authorizedUser) throws NotFoundException {
    final String calNetUID = authorizedUser ? "authorized" : "unauthorized";
    final CourseOffering c = createCourseOffering();
    addParticipation(c, calNetUID, false);
    final CapturePreferences preferences = new CapturePreferences();
    preferences.setRecordingType(RecordingType.screencast);
    preferences.setDelayPublishByDays(0);
    preferences.setRecordingAvailability(RecordingAvailability.publicCreativeCommons);
    //
    final QueryResult value = new QueryResult();
    final SObject sObject = new SObject();
    value.setRecords(new SObject[] {sObject});
    expect(connectorService.query(SalesforceQueryType.selectCourseOffering, anyObject(Map.class))).andReturn(value).once();
    expect(objectTransformer.getCourseOffering(sObject)).andReturn(c).once();
    //
    replay(connectorService, objectTransformer);
    courseManagementService.updateCapturePreferences(c.getCourseOfferingId(), "unauthorized", preferences);
  }

  private void addParticipation(final CourseOffering c, final String calNetUID, final boolean approved) {
    final Set<Participation> list = c.getParticipationSet() == null
        ? new HashSet<Participation>()
        : c.getParticipationSet();
    final Instructor instructor = new Instructor();
    instructor.setCalNetUID(calNetUID);
    list.add(new Participation(null, instructor, approved));
    c.setParticipationSet(list);
  }

  private CourseOffering createCourseOffering() {
    final String randomId = new Date().getTime() * Math.random() + "";
    final CourseOffering c = new CourseOffering();
    final CanonicalCourse cc = new CanonicalCourse();
    cc.setCcn(cc.hashCode());
    cc.setDepartment("Dept " + Math.random());
    cc.setTitle("Title " + Math.random());
    c.setCanonicalCourse(cc);
    c.setCourseOfferingId(randomId);
    final List<Participation> list = Collections.list(createParticipation(), createParticipation());
    c.setParticipationSet(new HashSet<Participation>(list));
    return c;
  }

  private Participation createParticipation() {
    final Instructor i = new Instructor();
    i.setCalNetUID(new Object().hashCode() + "");
    i.setDepartment("Dept " + Math.random());
    i.setEmail(i.getCalNetUID() + "@berkeley.edu");
    i.setFirstName("First name of " + i.getCalNetUID());
    i.setLastName("Last name of " + i.getCalNetUID());
    return new Participation(null, i, false);
  }

}
