/**
 *  Copyright 2009, 2010 The Regents of the University of California
 *  Licensed under the Educational Community License, Version 2.0
 *  (the "License"); you may not use this file except in compliance
 *  with the License. You may obtain a copy of the License at
 *
 *  http://www.osedu.org/licenses/ECL-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an "AS IS"
 *  BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 *  or implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 */
package org.opencastproject.participation.impl.persistence;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNull;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.junit.Ignore;
import org.opencastproject.participation.UnitTestUtils;
import org.opencastproject.participation.model.CanonicalCourse;
import org.opencastproject.participation.model.CourseData;
import org.opencastproject.participation.model.DayOfWeek;
import org.opencastproject.participation.model.Room;
import org.opencastproject.participation.model.Semester;

import org.apache.commons.lang.StringUtils;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * @author John Crossman
 */
@Ignore
public class UCBerkeleyCourseDatabaseImplTest {

  private ProvidesCourseCatalogData courseDatabase;

  @Before
  public void before() throws IOException {
    courseDatabase = new UCBerkeleyCourseDatabaseImpl(UnitTestUtils.getCourseDatabaseCredentials());
  }

  @Test
  public void testGetCourseOfferings() {
    final Set<CourseData> courseDataSet = courseDatabase.getCourseData(Semester.Fall, 2013);
    assertTrue(courseDataSet.size() > 0);
    //
    final Set<Integer> ccnNumbersFound = new HashSet<Integer>();
    for (final CourseData next : courseDataSet) {
      // NOTE: courseOfferingId is null because we do NOT need to push that value to Salesforce
      assertNull(next.getCourseOfferingId());
      final CanonicalCourse canonicalCourse = next.getCanonicalCourse();
      assertNotNull(canonicalCourse);
      final Integer ccn = canonicalCourse.getCcn();
      assertNotNull(ccn);
      assertTrue("list.add() returned false which means duplicate ccn found.", ccnNumbersFound.add(ccn));
      assertNotNull(canonicalCourse.getDepartment());
      assertNotNull(canonicalCourse.getTitle());
      //
      assertNotNull(next.getStartTime());
      assertNotNull(next.getEndTime());
      assertNotNull(next.getTerm().getSemester());
      assertNotNull(next.getTerm().getTermYear());
      final Room room = next.getRoom();
      assertNotNull(room);
      final String roomInfo = room.getBuilding() + "-" + ((room.getRoomNumber() == null) ? "[null]" : room.getRoomNumber());
      System.out.println(ccn + " : " + canonicalCourse.getTitle() + " in " + roomInfo);
      assertNotNull(StringUtils.trimToNull(room.getBuilding()));
      //
      final List<DayOfWeek> meetingDays = next.getMeetingDays();
      assertNotNull(meetingDays);
      assertFalse(meetingDays.isEmpty());
      assertNotNull(next.getSection());
      assertNotNull(next.getStudentCount());
    }
    assertEquals(ccnNumbersFound.size(), courseDataSet.size());
    System.out.println(ccnNumbersFound.size() + " CCNs found");
  }

}
