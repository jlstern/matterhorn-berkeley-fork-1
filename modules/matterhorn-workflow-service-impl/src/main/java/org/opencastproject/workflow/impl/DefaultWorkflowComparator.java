/**
 *  Copyright 2009, 2010 The Regents of the University of California
 *  Licensed under the Educational Community License, Version 2.0
 *  (the "License"); you may not use this file except in compliance
 *  with the License. You may obtain a copy of the License at
 *
 *  http://www.osedu.org/licenses/ECL-2.0
 *
 *  Unless required by applicable law or agreed to in writing,
 *  software distributed under the License is distributed on an "AS IS"
 *  BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 *  or implied. See the License for the specific language governing
 *  permissions and limitations under the License.
 *
 */
package org.opencastproject.workflow.impl;

import java.util.Comparator;

/**
 * @author John Crossman
 */
public class DefaultWorkflowComparator implements Comparator<String> {

  private final String defaultWorkflowID;

  /**
   * @param defaultWorkflowID Null not allowed
   */
  public DefaultWorkflowComparator(final String defaultWorkflowID) {
    this.defaultWorkflowID = defaultWorkflowID;
  }

  @Override
  public int compare(final String workflowID1, final String workflowID2) {
    final int result;
    if (workflowID1.equals(workflowID2)) {
      result = 0;
    } else if (defaultWorkflowID.equals(workflowID1)) {
      result = -1;
    } else if (defaultWorkflowID.equals(workflowID2)) {
      result = 1;
    } else {
      result = workflowID1.compareTo(workflowID2);
    }
    return result;
  }

}
