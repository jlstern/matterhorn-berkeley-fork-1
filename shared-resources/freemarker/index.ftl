<#function howManySignUpsSoFar courseOffering approvalStatus>
    <#assign set = courseOffering.participationSet />
    <#assign count = 0 />
    <#list set as participant>
        <#if (participant.approved == approvalStatus)>
            <#assign count = count + 1 />
        </#if>
    </#list>
    <#return count>
</#function>

<#function hasApproved courseOffering user>
    <#assign set = courseOffering.participationSet />
    <#list set as participant>
        <#if participant.approved && (user.userName == participant.instructor.calNetUID)>
            <#return true>
        </#if>
    </#list>
    <#return false>
</#function>

<#include "macros.ftl" >

<#assign isAdminUser = user?? && user.roles?? && (user.roles?seq_contains("ROLE_ADMIN")) />

<#--
Declare variables.
-->
<#if courseOffering??>
    <#assign signUpsSoFar = howManySignUpsSoFar(courseOffering, true) />
    <#assign undecidedCount = howManySignUpsSoFar(courseOffering, false) />
    <#assign thisUserApproved = hasApproved(courseOffering, user) />
    <#assign publishDelay = courseOffering.capturePreferences.delayPublishByDays />
    <#assign describePublishDelay><#if (publishDelay == 0)><@message "zeroPublishDelay" /><#else>${publishDelay} day${(publishDelay == 1)?string("","s")} after capture</#if></#assign>
    <#assign recordingAvailability = courseOffering.capturePreferences.recordingAvailability />
</#if>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title><@message "signUpPageTitle" /></title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <link rel="shortcut icon" href="/img/favicon.ico" />
        <link href="http://localhost/~john/matterhorn/css/sign-up.css" rel="stylesheet" type="text/css" />
        <link href="/css/jquery/jquery.fancybox.css" rel="stylesheet" type="text/css" />

        <script type="text/javascript" src="/js/angular/angular.min.js"></script>
        <script type="text/javascript" src="http://localhost/~john/matterhorn/js/sign-up.js"></script>
    </head>
    <body ng-app="">
        <div class="cct-container-main">
            <div id="ets_logo"><img src="/img/mh_logos/ets_logo.png"/></div>
            <h1><@message "signUpH1" /></h1>

            <#if error??>
                <#-- Display error and nothing else. -->
                <span id="warning" class="warning">${error}</span>

            <#elseif courseOffering??>
                <#assign room = courseOffering.room />
                <h2 class="coursetitle" id="coursetitle">
                    <#assign title = courseOffering.canonicalCourse.title />
                    <#assign startTimeMinutes>${(startTime.minutes < 10)?string('0' + startTime.minutes, startTime.minutes)}</#assign>
                    <#assign endTimeMinutes>${(endTime.minutes < 10)?string('0' + endTime.minutes, endTime.minutes)}</#assign>
                    <#if isAdminUser && courseOffering.salesforceID??>
                        <a href="https://cs1.salesforce.com/${courseOffering.salesforceID}" target="_blank">${title}</a>
                    <#else>
                        ${title}
                    </#if>
                    &nbsp;|&nbsp; ${room.building} ${room.roomNumber!""}<br/>
                    <#list courseOffering.meetingDays as meetingDay>${meetingDay}<#if meetingDay_has_next>,</#if> </#list>
                    ${startTime.hour}:${startTimeMinutes}${startTime.dayPeriod}-${endTime.hour}:${endTimeMinutes}${endTime.dayPeriod}
                </h2>
                <#if courseOffering.participationSet??>
                    <#--
                    ================================================================================================
                    List instructors, bolding when current user is match.
                    -->
                    <p id="instructorList">Instructor<#if (courseOffering.participationSet?size > 1)>s</#if>:
                        <#list courseOffering.participationSet as participant>
                            <#assign instructor = participant.instructor />
                            <#assign isCurrentUser = user.userName == instructor.calNetUID />
                            ${isCurrentUser?string("<b>","")}
                                ${instructor.firstName} ${instructor.lastName}
                            ${isCurrentUser?string("</b>","")}
                            (${participant.approved?string("","not yet ")}signed up)<#if participant_has_next>,</#if>
                        </#list>
                    </p>
                </#if>
                    <p id="webcastExplanation">
                        <@message "webcastExplanationShort" />
                        <span id="moretext"><a class="toggle" href="#typehelp">more</a></span>
                        <span id="lesstext" hidden><a class="toggle" href="#typehelp">less</a></span>
                    </p>
                    <p class="helptext" id="typehelp" style="display: none;">
                        <@message "webcastExplanationLong"/>
                    </p>
                <#if courseOffering.scheduled>
                    <#--
                    ================================================================================================
                    Recordings already scheduled.
                    -->
                    <div class="major-section">
                        <h3 id="settingsReview"><@message "reviewCurrentSettingsH3" /></h3>

                        <p id="standardSettings" class="instructionaltext" style="display: block;">
                            The following settings have been selected for capture, and the recordings have been scheduled.
                            Contact <a href="mailto:webcast@berkeley.edu">webcast@berkeley.edu</a> if you would like to make any changes to these settings.
                        </p>
                        <div id="inputSelectionAsText" style="display: block;">
                            Recordings type: Video with Audio
                        </div>
                        <div class="optionsAsText">
                            License: <@message courseOffering.capturePreferences.recordingAvailability />
                        </div>
                        <p><label>When recordings should be published:</label> <span id="describePublishDelay">${describePublishDelay}</span></p>
                    </div>
                <#elseif thisUserApproved>
                    <#--
                    ================================================================================================
                    This user approved earlier but other instructors have not yet responded.
                    -->
                    <p class="successMsg"><@message "youHaveAlreadySignedUp" /></p>

                    <div class="major-section">
                        <h3><@message "reviewCurrentSettingsH3" /></h3>
                        <span class="instructionaltext"><@message "youPreviouslyTheFollowing" /> <@message "contactUs" /></span>
                        <div class="optionsAsText" style="display: block;">
                            <p><label>Recordings type:</label> <span id="recordingType"><@message courseOffering.capturePreferences.recordingType /></span></p>
                            <p><label>How recordings should be made available:</label> <span id="recordingAvailability"><@message recordingAvailability /></span></p>
                            <p><label>When recordings should be published:</label> <span id="describePublishDelay">${describePublishDelay}</span></p>
                        </div>
                    </div>
                <#else>
                    <form action="/signUp" method="POST">
                        <#if (signUpsSoFar == 0)>
                            <div class="major-section">
                                <h3 class="inputSelectedHide">Choose what you want recorded:</h3>
                                <span class="optionsLabel inputSelectedShow" hidden="true" style="display: none;">Choose what you want recorded:</span>
                                <#assign videoCapableRoom = room.capability?lower_case?contains("video") />
                                <#--
                                <ol id="selectable" class="ui-selectable">
                                -->
                                    <#list recordingTypeList as recordingType>
                                        <#assign typeWithVideo = recordingType?lower_case?contains("video") />
                                        <#assign clickable = videoCapableRoom || !typeWithVideo />
                                        <#--
                                        <li id="${recordingType}" value="${recordingType}" class="ui-state-default ${recordingType} ui-selectee ui-selectable <#if clickable>ui-selected cursor-pointer</#if>" show="${recordingType}">
                                        -->
                                            <#if clickable>
                                                <div>
                                                    <input id="${recordingType}" type="radio" name="recordingType" value="${recordingType}"/>
                                                    <label for="${recordingType}">
                                                        <@message "${recordingType}" /> (<@message "${recordingType}Cost"/>)
                                                        - <a href="#${recordingType}DemoBox" class="fancybox">play demo</a>
                                                    </label>
                                                </div>
                                            </#if>
                                            <#--
                                            <span id="selectable-label" class="<#if clickable>cursor-pointer<#else>cursor-default</#if>">
                                                <@message "${recordingType}" />
                                                <@message "${recordingType}ButtonIcon" />
                                                <@message "${recordingType}Cost" />
                                            </span>
                                            -->
                                        <#--
                                        </li>
                                        -->
                                    </#list>
                                <#--
                                </ol>
                                -->
                                <#--
                                <br>
                                <ol id="playdemolinks">
                                    <li><a href="#audioDemoBox" class="fancybox">Play demo</a></li>
                                    <li><a href="#screenDemoBox" class="fancybox">Play demo</a></li>
                                    <li><a href="#videoDemoBox" class="fancybox">Play demo</a></li>
                                    <li><a href="#allDemoBox" class="fancybox">Play demo</a></li>
                                </ol>
                                -->
                                <div id="audioDemoBox" style="display:none;width:auto;">
                                    <iframe width="640" height="480" src="http://www.youtube.com/embed/xOXZPuSCAI0?rel=0" frameborder="0" allowfullscreen=""></iframe>
                                </div>
                                <div id="screenDemoBox" style="display:none;width:auto;">
                                    <iframe width="640" height="480" src="http://www.youtube.com/embed/myQhvHYW_2s?rel=0" frameborder="0" allowfullscreen=""></iframe>
                                </div>
                                <div id="videoDemoBox" style="display:none;width:auto;">
                                    <iframe width="640" height="360" src="http://www.youtube.com/embed/exo4-HoGZ3Q?rel=0" frameborder="0" allowfullscreen=""></iframe>
                                </div>
                                <div id="allDemoBox" style="display:none;width:auto;">
                                    <iframe width="640" height="360" src="http://www.youtube.com/embed/hs7BSi_r3nU?rel=0" frameborder="0" allowfullscreen=""></iframe>
                                </div>
                                <#--
                                <span id="inputSelectionSubmit" class="inputSelectedShow" hidden="true" style="display: none;">
                                    <input type="button" id="confirmDistributionOption" class="confirmChange" value="Save Change"> <a class="cancelChange" href="#">Cancel</a>
                                </span>
                                -->
                            </div>
                        </#if>
                        <div class="major-section">
                            <#--
                            ================================================================================================
                            This user has not yet decided. Or user is Admin.
                            -->
                            <h3 id="settingsReview"><#if (signUpsSoFar == 0)><@message "reviewOtherOptionsH3" /><#else><@message "reviewCurrentSettingsH3" /></#if></h3>
                            <span class="instructionaltext">
                                <@message "otherInstructorSignedUp" />
                            </span>
                            <h4>How recordings should be made available?</h4>
                            <#list recordingAvailabilityList as recordingAvailability>
                                <#assign radioId = "${recordingAvailability}Option" />
                                <input type="radio" name="recordingAvailability" class="radioItem" value="${recordingAvailability}" id="${radioId}"/>
                                <div class="radioLabel">
                                    <label for="${radioId}"><@message "${recordingAvailability}Details" /></label>
                                </div>
                                <div style="clear:both; margin-bottom: 2px"></div>
                            </#list>
                            <h4>When recordings should be published</h4>
                            <div class="radioLabel">
                                <input type="radio" class="radioItem" id="noPublishDelay" name="publishDelay" value="no" ng-model="selected"/>
                                <label for="publishDelayZero">As soon after capture as possible</label>
                            </div>
                            <div style="clear:both; margin-bottom: 2px"></div>
                            <div class="radioItem">
                                <input type="radio" id="publishDelay" name="publishDelay" value="yes" ng-model="selected"/>
                                &nbsp;
                                <label for="publishDelayMany">
                                    <select id="publishDelayDays" name="publishDelayDays" ng-disabled="(selected == 'no') || !selected">
                                        <option value="">Select...</option>
                                        <#list publishByDayOptions as publishByDayOption>
                                            <option value="${publishByDayOption}">${publishByDayOption}</option>
                                        </#list>
                                    </select> days after capture
                                </label>
                            </div>
                        </div>
                        <div class="major-section">
                            <#assign isLastApprovalNeeded = !thisUserApproved && (signUpsSoFar == courseOffering.participationSet?size - 1) />
                            <#assign mustAgreeToTerms = !isAdminUser />
                            <#if mustAgreeToTerms>
                                <input type="checkbox" name="agreeToTerms" id="agreeToTerms" ng-model="checked" />&nbsp;<label for="agree"><@message "agreeToTheTerms" /></label><br/>
                            </#if>
                            <input type="hidden" name="id" value="${courseOffering.courseOfferingId}" />
                            <input type="button" id="signUpButton" value="Sign Up" ng-disabled="<#if mustAgreeToTerms>! </#if>checked" onclick="submit()"/>
                            <#if isLastApprovalNeeded || isAdminUser>
                                <span class="warning">Submitting will schedule recordings!</span>
                            </#if>
                        </div>
                    </form>
                </#if>
            </#if>
        </div>
    </body>
</html>
